module.exports = function(sequelize, DataTypes) {
	return sequelize.define('User',{
		email: {
            type: DataTypes.STRING
        },
        password:{
        	type: DataTypes.STRING
        },

	}, {
        tableName: 'User',
        timestamps: false,
        underscored: true
    });
};