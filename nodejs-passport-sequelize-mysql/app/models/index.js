var Sequelize = require('sequelize'),
    config    = require(__dirname + '/../../config/database.json');

//initialyze sequelize
var sequelize = new Sequelize(config.development.database, config.development.username, config.development.password, {
  host   : config.development.host,
  charset:'utf8',
  collate:'utf8_general_ci',
  logging: false
});

//load models
var models = ['User'];
models.forEach(function(model) { module.exports[model] = sequelize.import(__dirname + '/' + model); });

//run migrations
sequelize.sync();

//exports the connection
module.exports.sequelize = sequelize; 