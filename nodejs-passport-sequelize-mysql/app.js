// set up ======================================================================
// get all the tools we need
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var session = require('cookie-session');
var passport = require('passport');
var flash 	 = require('connect-flash');
var routes = require('./routes/index');
var fs      = require('fs');

require('./config/passport')(passport); // pass passport for configuration

var app = express();


	app.set('port', process.env.PORT || 3000);
	app.set('views', path.join(__dirname, 'app/views'));
	app.use(logger('dev')) 
	app.use(cookieParser()); 
	app.use(bodyParser()); 

	app.set('view engine', 'ejs'); 
	app.use(session({
		keys : ['aaaaaaa', 'bbbbbbb']
	}));
	app.use(passport.initialize());
	app.use(passport.session()); 
	app.use(flash()); 

app.use('/', routes);

fs.readdirSync(__dirname + '/app/controllers/').forEach(function (file) {
  if(file.substr(-3) == '.js') {
      route = require(__dirname + '/app/controllers/' + file);
      route.controller(app, passport);
  }
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});